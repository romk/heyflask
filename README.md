# heyflask

A minimal Flask hello-world web application.


## Installation

```
$ pip install heyflask
```

## Run

```
$ flask --app=heyflask run
```

To test, execute in another terminal:
```
$ curl localhost:5000
```
Should see a response "Hey, Flask!"
